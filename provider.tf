provider "google" {
  credentials = file(var.credentials)
  project     = var.project_id
  region      = var.region
  version     = "=3.23.0"
}
