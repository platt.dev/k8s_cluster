terraform {
  required_version = "0.12.26"

  backend "gcs" {
    bucket  = "tf-state-platt-dev-k8s"
    prefix  = "k8s_cluster"
  }
}
